#!/usr/bin/env python
from bottle import route, static_file, run, view, template, redirect
import sys
import random 


recipe_list = ["http://allrecipes.com/recipe/banana-crumb-muffins/detail.aspx" , "http://allrecipes.com/Recipe/Too-Much-Chocolate-Cake/Detail.aspx?evt19=1&referringHubId=79", "http://allrecipes.com/Recipe/Peanut-Butter-Cup-Cookies/Detail.aspx?evt19=1&referringHubId=79", "http://chimeraobscura.com/mi/thats-a-wrap/", "http://www.bakersroyale.com/bars-and-cookie-bars/butterfinger-cookie-dough-cheesecake-bars/", "http://www.foodandwine.com/recipes/double-dark-chocolate-cupcakes-with-peanut-butter-filling", "http://www.countryliving.com/food-drinks/recipes/a4892/coffee-creme-brulee-recipe-clv0214/", "http://www.saveur.com/article/Recipes/German-Chocolate-Cake", "http://www.saveur.com/article/Recipes/Blackberry-Slump", "http://www.foodnetwork.com/recipes/patrick-and-gina-neely/coconut-cream-pie-recipe.html","http://www.seriouseats.com/recipes/2013/08/hummingbird-pudding-cake.html", "http://www.foodnetwork.com/recipes/ina-garten/outrageous-brownies-recipe3.html", "http://addapinch.com/cooking/the-best-chocolate-cake-recipe-ever/", "http://www.thisrawsomeveganlife.com/2011/11/sometimes-ya-just-need-fresh-baked.html#.VT1FOq1Viko", "http://www.jamesbeard.org/recipes/amys-simply-delicious-yellow-cake-pink-buttercream-frosting", "http://allrecipes.com/recipe/fresh-southern-peach-cobbler/", "http://www.foodnetwork.com/recipes/paula-deen/cinnamon-rolls-recipe.html", "http://lovelylittlekitchen.com/cookies-cream-cheesecake-cups", "http://www.averiecooks.com/2013/09/loaded-mm-oreo-cookie-bars.html", "http://www.cookingclassy.com/2012/02/monkey-bread-muffins-quite-possibly-the-best-thing-ive-ever-eaten/", "http://www.howsweeteats.com/2014/12/chocolate-cinnamon-babka-muffins/", "http://www.twopeasandtheirpod.com/apple-zucchini-muffins/", "http://www.handletheheat.com/old-fashioned-sour-cream-doughnuts/", "http://www.handletheheat.com/caramel-apple-streusel-pie/", "http://www.handletheheat.com/chocolate-pecan-tassies/", "http://www.handletheheat.com/creme-brulee-cheesecake-bars/", "http://www.howsweeteats.com/2013/02/dark-chocolate-fudge-merlot-cupcakes/", "http://www.foodnetwork.com/recipes/ina-garten/lemon-bars-recipe.html", "http://allrecipes.com/recipe/favorite-old-fashioned-gingerbread/", "http://www.finecooking.com/recipes/classic-apple-pie.aspx", "http://www.lepainquotidien.com/recipe/speculoos-tiramisu-recipe/#.VT1NeK3BzGc", "http://smittenkitchen.com/blog/2015/01/key-lime-pie/"]

@route('/')
def homepage():
    return template ('whatshouldibaketoday')

@route('/go')
def go():
    redirect (random.choice(recipe_list))

@route('/<path:path>')
def css(path):
    return static_file(path,root='./static')


if __name__ == "__main__":
    run(port=8080, debug=True, reloader=True)
else:
    import bottle
    global app
    app = bottle.default_app()