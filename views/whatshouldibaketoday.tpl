<html>
    <head>
        <title>
            What Should I Bake Today?
        </title>
        <link rel="stylesheet" type="text/css" href="whatshouldibaketoday.css">
        <link href='http://fonts.googleapis.com/css?family=Gloria+Hallelujah|Indie+Flower' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
        <script type="text/javascript">
//<![CDATA[
  (function() {
    var shr = document.createElement('script');
    shr.setAttribute('data-cfasync', 'false');
    shr.src = '//dsms0mj1bbhn4.cloudfront.net/assets/pub/shareaholic.js';
    shr.type = 'text/javascript'; shr.async = 'true';
    shr.onload = shr.onreadystatechange = function() {
      var rs = this.readyState;
      if (rs && rs != 'complete' && rs != 'loaded') return;
      var site_id = 'd92af07f6b85ff19283cdcaca7268f42';
      try { Shareaholic.init(site_id); } catch (e) {}
    };
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(shr, s);
  })();
//]]>
</script>
<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    
    <body>
        <h1>What Should I Bake Today?</h1>

        <div class="box">
            <h2> For those times when you just can't decide...</h2>
            <a href="/go" target="_blank">
                <div class= "button">
            ? 
                </div>
            </a>
            
                    
        <div class='shareaholic-canvas' data-app='share_buttons' data-app-id='17385724'></div>
        
        </div>
        
        
    <div>
        <div class="takingrefuge"> Also by Rebecca Allen: <a href="http://www.takingrefuge.org"> www.TakingRefuge.org </a> </div>
        
        <div class = "footer">
        All content copyright Rebecca Allen &copy; 2015 <br/> All rights reserved 
        </div>
    </div>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-62273180-1', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>