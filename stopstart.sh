#!/bin/bash

if [ -f .lock ]; then
	kill $(cat .lock)
fi

pip install -r pipfile
gunicorn -w 2 -p .lock -D -b 127.0.0.1:9001 whatshouldibaketoday:app